


; PIC12F1840 Configuration Bit Settings
#include "p12f1840.inc"


;config bits: internal osc.
; CONFIG1
; __config 0x2FA4
 __CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _PWRTE_OFF & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _BOREN_ON & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_ON
; CONFIG2
; __config 0x1FFF
 __CONFIG _CONFIG2, _WRT_OFF & _PLLEN_ON & _STVREN_ON & _BORV_LO & _LVP_OFF


 ;wiring PICf1840
 ;   +VCC     1 XXXX 8   GND
 ;            2 XXXX 7   ISC-dat
 ;   LEDstr   3 XXXX 6   ISC-clk
 ; Reset/Butt 4 XXXX 5   P2 (alive)
 
 ; currently set for MCH stuff LED strips.
 ; adding EEPROM last state to cycle when powering up.


NUM_LEDS = 0x30	; max 0x30!!!

ANIMS_NUMBER = .9
BUTTON_TM_THRES = .100
DEF_ANIM = .2

; vars on all banks
ml_temp          = 0x70		; used by any subrt.
ml_temp2         = 0x71		; used by any subrt.
ml_temp3         = 0x72		; used by 'bigger' subrt.
ml_temp4         = 0x73		; used by 'bigger' subrt.
ml_timeCount     = 0x74		; advances 50/sec
ml_ledSequenceCount = 0x76
ml_txCount       = 0x78
ml_count         = 0x7A
ml_bitcount      = 0x7B
ml_stackptr      = 0x7C

; vars bound to bank 0
l_Al             = 0x20
l_Ah             = 0x21
l_Bl             = 0x22
l_Bh             = 0x23
l_Cl             = 0x24
l_Ch             = 0x25

l_button         = 0x26
l_anim_no        = 0x27	; cycled on button (or powercycle)

l_secondsL       = 0x28
l_secondsH       = 0x29

l_rndL           = 0x2A
l_rndH           = 0x2B

l_state          = 0x2C ; 0x10 bytes. Zeroed on switch.

l_colR         = 0x3C
l_colG         = 0x3D
l_colB         = 0x3E
l_colH         = 0x3F
l_colS         = 0x40
l_colV         = 0x41

l_pos          = 0x42  ; LED-pos in calc-loop of patterns.
l_X            = 0x43

l_scratch  = 0x44	; 0x08 bytes



; PIC 12F1840
portA_TX = 0
;portA_I2Cc = 1
;portA_I2Cd = 2
portA_alive = 2
portA_button = 3
portA_pin = 4

bufferLED = 0x2030		; size 3*NUM_LEDS, max 0xB4 or 0x3C LEDs (=48d)

stackarea = 0x20F0      ; downward. must be area so low-byte does not wrap.



	org 0
	goto skipToSetup

	org 4
	retfie

skipToSetup:


	; oscillator setup ..... To get 32MHz internal, FOSC=100 (INTOSC), SCS=00 (source=INTOSC), IRCF=1110 (8MHz), SPLLEN=1 (PLLEN)
	movlw 0xF0	; PLL on, int osc 8MHz, clockselect by config.
	banksel OSCCON
	movwf OSCCON
	banksel 0

	movlw 0xC8
pauseOsc:
	nop
	nop
	decfsz WREG,0
	bra pauseOsc

	; port setup
	banksel ANSELA
	clrf ANSELA
	banksel WPUA
	movlw 0xFF
	movwf WPUA
	banksel LATA
	bcf LATA,portA_pin
	bcf LATA,portA_alive
	banksel TRISA
	movlw 0xFF-(1<<portA_pin)-(1<<portA_TX)-(1<<portA_alive)
	movwf TRISA
	banksel OPTION_REG
	bcf OPTION_REG,7	; enable pull-ups with WPUx
	banksel 0

	call get_state_from_EEPROM



	; timer2 is the time-clock. always running without ints.  64*10*250=160000 -> 50 Hz (@8MHz).
	; value is polled from mainloop. It wraps slow enough for this.
	; timer 2 period is 160000 cycles.
	banksel TMR2
	movlw 0xF9	; 250-1
	movwf PR2
	movlw 0x4F  ; prescale=64, enable, postscale=1:10
	movwf T2CON
	clrf ml_timeCount
	banksel PIE1
	bcf PIE1,1  ; disable int


	; startup-pause
	movlw 0x07
	movwf ml_temp4
	clrf ml_temp3
	clrf WREG
startPause:
	nop
	nop
	decfsz WREG,0
	bra startPause
	decfsz ml_temp3,1
	bra startPause
	decfsz ml_temp4,1
	bra startPause


	; Vars setup
	banksel 0
	movlw low stackarea
	movwf ml_stackptr
	clrf ml_txCount
	clrf ml_timeCount
	clrf l_secondsL
	clrf l_secondsH
	movlw 0xFF
	movwf l_rndL
	movwf l_rndH

	call clr_state

	movlw 0 ; was 0xFF
	movwf l_button
	; movlw DEF_ANIM
	; movwf l_anim_no


mainloop:
	banksel 0

	; timer tick
	banksel 0
	btfss PIR1,1		; TMR2IF
	bra $+3
	bcf PIR1,1		; TMR2IF
	incf ml_timeCount,1

	; check/increment l_seconds
	banksel 0
	movlw 0x32
	subwf ml_timeCount,0
	btfsc STATUS,C
	movwf ml_timeCount
	btfsc STATUS,C
	incf l_secondsH,1
	; calc 5.125*timecount
	lslf ml_timeCount,0
	lslf WREG,0
	addwf ml_timeCount,0
	movwf l_secondsL
	lsrf ml_timeCount,0
	lsrf WREG,0
	lsrf WREG,0
	addwf l_secondsL,1

	; alive-LED
	movf l_secondsL,0
	movwf ml_temp
	banksel TRISA
	movf TRISA,0
	bcf WREG,portA_alive
	btfsc ml_temp,7
	bsf WREG,portA_alive
	movwf TRISA
	banksel 0




	nop

	call call_anim

	banksel 0

	call sendLEDstrip

	; poll button
	btfsc PORTA,portA_button
	bra _main__buttPrs
	; is up. increase state, cap at 0xFF
	incf l_button,1
	btfsc STATUS,Z
	decf l_button,1
	bra _main__buttDone
_main__buttPrs:
	movlw BUTTON_TM_THRES
	subwf l_button,0    ; l_button >= BUTTON_TM_THRES  ?
	clrf l_button
	btfss STATUS,C
	bra _main__buttDone
	; change active animation
	incf l_anim_no,1
	movlw ANIMS_NUMBER
	subwf l_anim_no,0   ; l_anim_no < ANIMS_NUMBER ?
	btfsc STATUS,C
	clrf l_anim_no
	call clr_state

_main__buttDone:


	nop
	bra mainloop

clr_state:
	clrf l_state+0x00
	clrf l_state+0x01
	clrf l_state+0x02
	clrf l_state+0x03
	clrf l_state+0x04
	clrf l_state+0x05
	clrf l_state+0x06
	clrf l_state+0x07
	clrf l_state+0x08
	clrf l_state+0x09
	clrf l_state+0x0A
	clrf l_state+0x0B
	clrf l_state+0x0C
	clrf l_state+0x0D
	clrf l_state+0x0E
	clrf l_state+0x0F
	return

call_anim:
	movf l_anim_no,0
	andlw 0x0F
	brw

;	goto gen_pattern0_null
	goto gen_pattern1
	goto gen_pattern2
	goto gen_pattern3

	goto gen_pattern4
	goto gen_pattern5
	goto gen_pattern6
	goto gen_pattern7_huescroll

	goto gen_pattern8_rob_spk
	goto gen_pattern1
	goto gen_pattern2
	goto gen_pattern3

	goto gen_pattern4
	goto gen_pattern5
	goto gen_pattern6
	goto gen_pattern7_huescroll

	goto gen_pattern8_rob_spk
	goto gen_pattern1
	goto gen_pattern2
	return

rnd:
	banksel 0
	movlw 3
	movwf ml_temp
_rnd__lop:
	lsrf l_rndH,1
	rrf l_rndL,1
	btfss STATUS,C
	bra $+5
	movlw 0xBF  ; reverse of poly 0x15DFD
	xorwf l_rndH,1
	movlw 0xBA
	xorwf l_rndL,1
	nop
	decfsz ml_temp,1
	bra _rnd__lop
	movf l_rndL,0
	return

get_state_from_EEPROM:
	; get.
	banksel EEADRL
	movlw 0x02
	movwf EEADRL
	movlw 0
	movwf EEADRH
	bcf EECON1,6	; CFGS
	bcf EECON1,7	; EEPGD
	bsf EECON1,0	; RD
	movf EEDATL,0
	banksel 0
	movwf l_anim_no

	; increase
	incf l_anim_no,1
	; wrap
;	movlw ANIMS_NUMBER
;	subwf l_anim_no,0   ; l_anim_no < ANIMS_NUMBER ?
;	btfsc STATUS,C
;	clrf l_anim_no

	; write back
	banksel EEADRL
	movlw 0x02
	movwf EEADRL
	movlw 0
	movwf EEADRH
	banksel 0
	movf l_anim_no,0
	banksel EEADRL
	movwf EEDATL
	bcf EECON1,6	; CFGS
	bcf EECON1,7	; EEPGD
	bsf EECON1,2	; WREN
	movlw 0x55
	movwf EECON2
	movlw 0xaa
	movwf EECON2
	bsf EECON1,1	; WR
	nop
	nop
	bcf EECON1,2	; WREN
	nop
	btfsc EECON1,1	; WR
	goto $-2
	; done
	banksel 0
	return


;====================================================================

#include "LEDstrip.asm"

;====================================================================

; stack (uses FSR0 and  ml_temp)
start_stack:
	movlw high stackarea
	movwf FSR0H
	movf ml_stackptr,0
	movwf FSR0L
	return

done_stack:
	movf FSR0L,0
	movwf ml_stackptr
	return

push_stack:
	movwi --FSR0
	return

pop_stack:
	moviw FSR0++
	return

;====================================================================
; math stuff.
;====================================================================

#include "../shared/mathFuncs.asm"

;====================================================================

	; pattern generators
#include "pattern0_null.asm"
#include "pattern1_kraftwerk.asm"
#include "pattern2_walkwhite.asm"
#include "pattern3_kit.asm"
#include "pattern4_droppers.asm"
#include "pattern5_huecycle.asm"
#include "pattern6_brightflash.asm"
#include "pattern7_huescroll.asm"
#include "pattern8_spk.asm"

#include "colorful.asm"

;====================================================================

	org 0x800

#include "../shared/mathTables.asm"



	end
