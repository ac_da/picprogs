


; PIC12F1840 Configuration Bit Settings
#include "p12f1840.inc"


; pins
;
; 1 ++ power
; 2 A5 LED switch output
; 3 A4 TX output
; 4 A3 reed-switch input
; 5 A2 alive-led
; 8 -- GND




;config bits: internal osc.
; CONFIG1
; __config 0x9A4
 __CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _PWRTE_OFF & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF
; CONFIG2
; __config 0x1FFF
 __CONFIG _CONFIG2, _WRT_OFF & _PLLEN_ON & _STVREN_ON & _BORV_LO & _LVP_OFF




LIGHT_TICKS = .30
BUTTON_TM_THRES = .100

; vars on all banks
ml_temp          = 0x70		; used by any subrt.
ml_temp2         = 0x71		; used by any subrt.
ml_temp3         = 0x72		; used by 'bigger' subrt.
ml_temp4         = 0x73		; used by 'bigger' subrt.
ml_timeCount     = 0x74		; advances 50/sec

ml_count         = 0x7A


; vars bound to bank 0
l_Al             = 0x20
l_Ah             = 0x21
l_Bl             = 0x22
l_Bh             = 0x23
l_Cl             = 0x24
l_Ch             = 0x25

l_button         = 0x28

l_secondsL       = 0x2A
l_secondsH       = 0x2B
l_nextMeasure    = 0x2C	; compared against l_secondsH
l_light_ticks    = 0x2D


l_state          = 0x30 ; 0x10 bytes. Zeroed on switch.


l_pos          = 0x47  ; LED-pos in calc-loop of patterns.



; PIC 12F1840
portA_ledctlC = 0  ; pin 7
portA_ledctlD = 1  ; pin 6
portA_alive = 2   ; pin 5
;portA_button = 3  ; pin 4
portA_reedsens = 3; pin 4
portA_ledctlB = 4  ; pin 3
portA_ledctlA = 5  ; pin 2


	org 0
	goto skipToSetup

	org 4
	retfie

skipToSetup:


	; oscillator setup ..... To get 4MHz internal, FOSC=100 (INTOSC), SCS=00 (source=INTOSC), IRCF=1101 (4MHz), SPLLEN=0 (PLLEN)
	movlw 0x68	; PLL off, int osc 4MHz, clockselect by config.
	banksel OSCCON
	movwf OSCCON
	banksel 0

	movlw 0xC8
pauseOsc:
	nop
	nop
	decfsz WREG,0
	bra pauseOsc

	; port setup
	banksel ANSELA
	clrf ANSELA
	banksel WPUA
	movlw 0xFF
	movwf WPUA
	banksel LATA
	bcf LATA,portA_ledctlA
	bcf LATA,portA_ledctlB
	bcf LATA,portA_ledctlC
	bcf LATA,portA_ledctlD
	bcf LATA,portA_alive
	banksel TRISA
	movlw 0xFF-(1<<portA_ledctlA)-(1<<portA_ledctlB)-(1<<portA_ledctlC)-(1<<portA_ledctlD)-(1<<portA_alive)
	movwf TRISA
	banksel OPTION_REG
	bcf OPTION_REG,7	; enable pull-ups with WPUx
	banksel 0


	; timer2 is the time-clock. always running without ints.  16*5*250=20000 -> 50 Hz (@1MHz).
	; value is polled from mainloop. It wraps slow enough for this.
	; timer 2 period is 160000 cycles.
	banksel TMR2
	movlw 0xF9	; 250-1
	movwf PR2
	movlw 0x26  ; prescale=16, enable, postscale=1:5
	movwf T2CON
	clrf ml_timeCount
	banksel PIE1
	bcf PIE1,1  ; disable int


	; startup-pause
	movlw 0x07
	movwf ml_temp4
	clrf ml_temp3
	clrf WREG
startPause:
	nop
	nop
	decfsz WREG,0
	bra startPause
	decfsz ml_temp3,1
	bra startPause
	decfsz ml_temp4,1
	bra startPause


	; Vars setup
	banksel 0
	clrf ml_timeCount
	clrf l_secondsL
	clrf l_secondsH
	clrf l_nextMeasure
	clrf l_light_ticks
	movlw 0xFF

	movlw 0 ; was 0xFF
	movwf l_button


mainloop:
	banksel 0

	; timer tick
	banksel 0
	btfss PIR1,1		; TMR2IF
	bra _no_timerA
	bcf PIR1,1		; TMR2IF
	incf ml_timeCount,1

	movf l_light_ticks,0
	btfsc STATUS,Z
	bra _no_timerA
	; timer is running.
	decf l_light_ticks,1
	btfss STATUS,Z
	bra _no_timerA

	banksel LATA
	bcf LATA,portA_ledctlA
	bcf LATA,portA_ledctlB
	bcf LATA,portA_ledctlC
	bcf LATA,portA_ledctlD
	banksel 0


_no_timerA:
	; check/increment l_seconds
	banksel 0
	movlw 0x32
	subwf ml_timeCount,0
	btfsc STATUS,C
	movwf ml_timeCount
	btfsc STATUS,C
	incf l_secondsH,1
	; calc 5.125*timecount
	lslf ml_timeCount,0
	lslf WREG,0
	addwf ml_timeCount,0
	movwf l_secondsL
	lsrf ml_timeCount,0
	lsrf WREG,0
	lsrf WREG,0
	addwf l_secondsL,1

	; alive-LED
	movf l_secondsL,0
	movwf ml_temp
	banksel TRISA
	movf TRISA,0
	bcf WREG,portA_alive
	btfsc ml_temp,7
	bsf WREG,portA_alive
	movwf TRISA
	banksel 0

	; poll input sensor.
	btfsc PORTA,portA_reedsens
	bra _reed_not_pulled

	movlw LIGHT_TICKS
	movwf l_light_ticks

	banksel LATA
	bsf LATA,portA_ledctlA
	bsf LATA,portA_ledctlB
	bsf LATA,portA_ledctlC
	bsf LATA,portA_ledctlD
	banksel 0

_reed_not_pulled:


	nop

	banksel 0

	; poll button
;;;;	btfsc PORTA,portA_button  ; button disabled
;;;;	bra _main__buttPrs
	; is up. increase state, cap at 0xFF
	incf l_button,1
	btfsc STATUS,Z
	decf l_button,1
	bra _main__buttDone
_main__buttPrs:
	movlw BUTTON_TM_THRES
	subwf l_button,0    ; l_button >= BUTTON_TM_THRES  ?
	clrf l_button
	btfss STATUS,C
	bra _main__buttDone
	; button was pressed. Do something.
	nop

_main__buttDone:


	nop
	bra mainloop


	end
